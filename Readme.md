# Application de Détection du Diabète Type 2

Cette application vise à aider les professionnels de santé à identifier les patients à risque de développer un diabète de type 2. Elle est conçue avec une architecture de microservices en utilisant Spring Boot et Spring Cloud Gateway. Chaque microservice est dockerisé pour faciliter le déploiement et la maintenance.

## Architecture

- **Microservice d'Authentification** : Permet de récupérer un bearer token, à partir d'un nom d'utilisateur et d'un mot de passe, ce token sert à s'authentifier à chaque micro service.
- **Microservice Patient** : Gère les données personnelles des patients.
- **Microservice de Notes** : Permet aux médecins d'ajouter des observations sur les patients.
- **Microservice de Risque de Diabète** : Évalue le risque de diabète basé sur les notes.
- **Gateway** : Route les requêtes aux différents microservices.

## Sécurité et Conformité

### Normalisation des Données

Les bases de données sont normalisées en 3NF pour assurer la qualité des données.
1. Toutes ses colonnes contiennent des valeurs atomiques, c’est-à-dire chaque colonne ne contient qu'une seule valeur pour chaque ligne, sans ensembles ou listes de valeurs.
2. Toutes les colonnes sont dépendantes de la clé primaire, c’est-à-dire que chaque colonne doit dépendre de la clé primaire de la table.
3. Tous ses attributs non-clés ne dépendent que de la clé primaire et non d’autres attributs non-clés.

### Sécurité des Données
Mise en place d'un système d'authentification robuste avec Spring Security pour protéger l'accès aux données des patients.

## Docker

Chaque microservice est conteneurisé avec Docker, permettant une scalabilité et une gestion efficace des ressources.

## Green Code

### Principes de base pour une programmation écoresponsable

1. **Optimisation des Requêtes** : Minimiser les requêtes inutiles et optimiser les requêtes existantes pour réduire la charge sur les serveurs et la consommation d'énergie.
2. **Utilisation Efficace des Ressources** : Utiliser des algorithmes et des structures de données efficaces pour réduire le temps de calcul et la consommation de ressources.
3. **Efficacité de Docker** : Utiliser des images Docker légères pour réduire la consommation d'énergie lors du déploiement et de l'exécution des microservices.
4. **Gestion de la Mémoire** : Optimiser la gestion de la mémoire dans les applications pour éviter la surutilisation des ressources système.
5. **Économie de Bande Passante** : Compresser les données transmises et utiliser des formats de données efficaces pour minimiser l'utilisation de la bande passante.

### Suggestions spécifiques pour le projet

1. **Optimisation des Requêtes** : Réduire le nombre de microservices et de base de données, les services patients, notes et risque pourraient être combinés en un seul service, avec une seul base de donnée relationnel.
2. **Utilisation Efficace des Ressources** : Implémenter des technologies de caching (comme Redis) pour les données fréquemment demandées afin de réduire le nombre de requêtes à la base de données et diminuer la charge sur les ressources du serveur. 
Nous pourrions également stocker, le résultat de l'évaluation du risque, dans une base de données, afin que le calcul ne soit pas refait à chaque fois que l'on souhaite afficher les patients.
3. **Efficacité de Docker** : Utiliser des images Docker multistage, par exemple, une image pour le build de l'application, avec, notamment, maven et un JDK d'installer, et une image pour l'exécution de l'application, avec seulement le jar de l'application, et un JRE.
4. **Gestion de la Mémoire** : Mettre en œuvre des pratiques de profilage de la mémoire pour identifier et corriger les fuites de mémoire, garantissant une utilisation optimale de la mémoire vive. Des outils comme VisualVM pourrait être utile.
5. **Économie de Bande Passante** : Minimiser la taille des données transmises en implémentant systématiquement, une pagination côté serveur.

## Installation et Déploiement

1. Cloner le repository et les sous-modules :
   ```bash
   git clone --recurse-submodules https://gitlab.com/medilabo/project-9.git
    ```
   
2. Lancer les services avec Docker Compose :
   ```bash
   docker-compose up --build
   ```
   
3. Accéder à l'application :
   - Ouvrir un navigateur et accéder à l'URL suivante : [http://localhost:8000]

4. Authentification :
   - Nom d'utilisateur : user
   - Mot de passe : password